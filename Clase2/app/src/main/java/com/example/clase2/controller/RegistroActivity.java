package com.example.clase2.controller;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.clase2.Globales.Global;
import com.example.clase2.R;

public class RegistroActivity extends AppCompatActivity {

    TextView etNombre, etEmail, etPassword, etCiudad, etFechaNacimiento, etTelefono;
    Button btnRegistro;

    Global global = new Global();

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        etNombre = findViewById(R.id.etNombre);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        btnRegistro = findViewById(R.id.btnRegistro);
        etCiudad = findViewById(R.id.etCiudad);
        etFechaNacimiento = findViewById(R.id.etFechaNacimiento);
        etTelefono = findViewById(R.id.etTelefono);

        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etNombre.getText().toString().isEmpty() || etPassword.getText().toString().isEmpty() ||
                        etEmail.getText().toString().isEmpty() || etTelefono.getText().toString().isEmpty() ||
                        etFechaNacimiento.getText().toString().isEmpty() || etCiudad.getText().toString().isEmpty()){
                    Toast toast = Toast.makeText(RegistroActivity.this, "Alguno de los campos esta vacío",
                            Toast.LENGTH_SHORT);
                    toast.show();

                } else {
                    global.nombre = etNombre.getText().toString();
                    global.email = etEmail.getText().toString();
                    global.password = etPassword.getText().toString();
                    global.telefono = etTelefono.getText().toString();
                    global.fechaNacimiento = etFechaNacimiento.getText().toString();
                    global.ciudad = etCiudad.getText().toString();
                    Toast toast = Toast.makeText(RegistroActivity.this, "Usuario Registrado",
                            Toast.LENGTH_SHORT);
                    toast.show();
                    Intent intent = new Intent(RegistroActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
            }
        });

    }
}